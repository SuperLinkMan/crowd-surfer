﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardController : MonoBehaviour {

	public float minGroundTime = 1f;
	public float jumpSpeed = 5f;
	public float maxHeight = 1f;

    private Vector2 initialPosition;
	private float direction;
	private float initialHeight;
	private bool falling;
	private float timeGrounded;
	private Rigidbody2D rb;
	private SpriteRenderer sr;
	public Transform targetTransform;

	void Awake() {
        initialPosition = transform.position;
		rb = GetComponent<Rigidbody2D>();
		sr = GetComponent<SpriteRenderer>();
		Reset ();
	}

	void Start() {
		targetTransform = GameObject.FindGameObjectWithTag("Player").transform;
	}
    
    public void Reset () {
        transform.position = initialPosition;
		direction = 2f;
		if (Mathf.Sign(transform.position.x) < 0f) {
			sr.flipX = false;
			direction *= -1f;
		} else {
			sr.flipX = true;
		}
        initialHeight = transform.position.y + 0.0001f;
        falling = false;
        timeGrounded = 0f;
    }   

	void FixedUpdate () {
		Vector3 movement = Vector3.right * direction * Time.fixedDeltaTime;

		if (transform.position.y > initialHeight + maxHeight) {
			falling = true;
			timeGrounded = 0f;
		} else if (transform.position.y < initialHeight) {
			falling = false;
			timeGrounded += Time.fixedDeltaTime;
		}

		if (falling) {
			movement -= Vector3.up * Time.fixedDeltaTime * jumpSpeed;
		} else if (timeGrounded > minGroundTime && transform.position.x > targetTransform.position.x - 1 && transform.position.x < targetTransform.position.x + 1) {
			// jump if near (on x-axis) to targetTrasform
			movement += Vector3.up * Time.fixedDeltaTime * jumpSpeed;
		} else if (transform.position.y > initialHeight) {
			falling = true;
			timeGrounded = 0f;
			movement -= Vector3.up * Time.fixedDeltaTime * jumpSpeed;
		}
		rb.MovePosition(transform.position + movement);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Wall")) {
			Flip();
		}
	}
		
	void Flip() {
		direction = -direction;
		sr.flipX = !sr.flipX;
	}

}
