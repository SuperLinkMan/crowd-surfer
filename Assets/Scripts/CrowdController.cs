﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdController : MonoBehaviour {

	public GameObject[] people;
	public Sprite[] spritesRow1;
	public Sprite[] spritesRow2;
	public Sprite[] spritesRow3;

	public int crowdSize = 32;
	public float initialHeightDifference = 0.5f;
	public float raiseHeight = 1f;
	public float raiseSpeed = 10f;
	public float inputEffectRange = 2f;

	public float animHeight = 0.1f;

	// keys must be entered in order from left to right of the crowd
	public string[] inputs = {"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"};
    public int minimumMidiKeyValue = 48;
    public int maximumMidiKeyValue = 72;

	private float maxX;
	private float minX;
	private float[] initialHeights;
	private GameObject[][] crowdGoers;

	private Dictionary<string, float> inputPoints; // the points on the x axis that should move the crowd from
    private Dictionary<int, float> midiInputPoints; // The points on the x axis that should move the crowd from for the midi controller

	private int row1Index = 0;
	private int row2Index = 1;
	private int row3Index = 2;

	void Awake () {
		Vector2 originPosition = transform.position;
		maxX = originPosition.x + transform.localScale.x / 2;
		minX = originPosition.x - transform.localScale.x / 2;
		initialHeights = new float[] {(transform.position.y - initialHeightDifference), transform.position.y, (transform.position.y + initialHeightDifference)};

		crowdGoers = new GameObject[][] {new GameObject[crowdSize], new GameObject[crowdSize], new GameObject[crowdSize]};

		float xDifference = transform.localScale.x / (inputs.Length - 1);
		inputPoints = new Dictionary<string, float>();
		for (int i = 0; i < inputs.Length; i++) {
			inputPoints.Add(inputs[i], i * xDifference + minX);
		}
        
        float xDifferenceMidi = transform.localScale.x / (maximumMidiKeyValue - minimumMidiKeyValue);
        midiInputPoints = new Dictionary<int, float>();
        for (int i = minimumMidiKeyValue; i <= maximumMidiKeyValue; i++) {
            midiInputPoints.Add(i, (i - minimumMidiKeyValue) * xDifferenceMidi + minX);
        }
	}

	void Start () {
		CreateCrowd();
	}
		
	void FixedUpdate () {

		for (int row = 0; row < crowdGoers.Length; row++) {
			foreach (GameObject crowdGoer in crowdGoers[row]) {
				PersonProperties properties = crowdGoer.GetComponent<PersonProperties>();
				properties.maxHeight = initialHeights[row];
				properties.jumping = false;
			}
		}

		// keyboard input
		foreach (string key in inputs) {
			if (Input.GetKey(key)) {
				RaiseCrowd(inputPoints[key]);
			}
		}
        
		// midi input
        for (int key = minimumMidiKeyValue; key <= maximumMidiKeyValue; key++) {
			try {
				if (MidiJack.MidiMaster.GetKey(key) > 0.0f) {
					RaiseCrowd(midiInputPoints[key]);
	            }
			} catch {} // swallow exception
        }

		// touch input
		for (int i = 0; i < Input.touchCount; ++i) {
			// transform from screen position to game position
			RaiseCrowd (Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position).x);
		}
			
		// bring crowdGoers back down who are above their maxHeight
		for (int row = 0; row < crowdGoers.Length; row++) {
			foreach (GameObject crowdGoer in crowdGoers[row]) {
				PersonProperties properties = crowdGoer.GetComponent<PersonProperties>();

				if (!properties.jumping) {
					if (properties.animUp) {
						if (crowdGoer.transform.position.y > initialHeights[row] + animHeight) {
							// move back down
							properties.animUp = false;
						} else {
							// move up
							Rigidbody2D rb = crowdGoer.GetComponent<Rigidbody2D>();
							rb.MovePosition(crowdGoer.transform.position + (Vector3.up * Time.fixedDeltaTime));
						}
					} else {
						if (crowdGoer.transform.position.y < initialHeights[row]) {
							properties.animUp = true;
						} else {
							// move down
							Rigidbody2D rb = crowdGoer.GetComponent<Rigidbody2D>();
							rb.MovePosition(crowdGoer.transform.position - (Vector3.up * Time.fixedDeltaTime));
						}
					}
				}

//				if (crowdGoer.transform.position.y > properties.maxHeight) {
//					properties.jumping = false;
//
//					float speed = -(raiseSpeed * (properties.maxHeight / raiseHeight)) / 4;
//					float newHeight = Time.fixedDeltaTime * speed;
//					Rigidbody2D rb = crowdGoer.GetComponent<Rigidbody2D>();
//					rb.MovePosition(crowdGoer.transform.position - (Vector3.up * newHeight));
//				}
			}
		}
	}

	void CreateCrowd() {
		Vector2 randomPosition;
		for (int i = 0; i < crowdSize; i++) {
			randomPosition = new Vector2 (Random.Range(minX, maxX), initialHeights[row1Index]);
			GameObject crowdGoer = Instantiate(people[0], randomPosition, Quaternion.identity);
			SpriteRenderer sprite = crowdGoer.GetComponent<SpriteRenderer>();
			sprite.sprite = spritesRow1[Random.Range(0, spritesRow1.Length)];
			sprite.sortingLayerName = "Row 1";
			crowdGoers[row1Index][i] = crowdGoer;

			randomPosition = new Vector2 (Random.Range(minX, maxX), initialHeights[row2Index]);
			crowdGoer = Instantiate(people[1], randomPosition, Quaternion.identity);
			sprite = crowdGoer.GetComponent<SpriteRenderer>();
			sprite.sprite = spritesRow2[Random.Range(0, spritesRow2.Length)];
			sprite.sortingLayerName = "Row 2";
			crowdGoers[row2Index][i] = crowdGoer;

			randomPosition = new Vector2 (Random.Range(minX, maxX), initialHeights[row3Index]);
			crowdGoer = Instantiate(people[2], randomPosition, Quaternion.identity);
			sprite = crowdGoer.GetComponent<SpriteRenderer>();
			sprite.sprite = spritesRow3[Random.Range(0, spritesRow3.Length)];
			sprite.sortingLayerName = "Row 3";
			crowdGoers[row3Index][i] = crowdGoer;
		}
	}

	void RaiseCrowd(float xPos) {
		for (int row = 0; row < crowdGoers.Length; row++) {
			foreach (GameObject crowdGoer in crowdGoers[row]) {
				
				PersonProperties properties = crowdGoer.GetComponent<PersonProperties>();

				// is crowdGoer in range of our inputEffectRange
				if (crowdGoer.transform.position.x > xPos - inputEffectRange*0.9f && crowdGoer.transform.position.x < xPos + inputEffectRange*0.9f) {
					float maxHeight = raiseHeight * (inputEffectRange - Mathf.Abs(xPos - crowdGoer.transform.position.x));
					float speed = raiseSpeed * (maxHeight / raiseHeight);

					properties.maxHeight = maxHeight;
					properties.jumping = true;
					properties.animUp = false;

					if (crowdGoer.transform.position.y < initialHeights[row] + maxHeight) {
						Rigidbody2D rb = crowdGoer.GetComponent<Rigidbody2D>();
						rb.MovePosition(crowdGoer.transform.position + Vector3.up * Time.fixedDeltaTime * speed);
					}
				} else {
					properties.jumping = false;
				}
			}
		}
	}

}
