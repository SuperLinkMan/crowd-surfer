﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public float SmoothTime = 8.0f;
    
    public GameObject target;
    
    private GameObject initialTarget;
    
    void Start () {
        initialTarget = target;
    }
    
    // Update is called once per frame
    void Update () {
        if (target == null || !target.activeSelf) {
            target = initialTarget;
        }
        
        Vector3 cameraPosition = transform.position;
        
        cameraPosition.x = target.transform.position.x;
        cameraPosition.y = target.transform.position.y;
        cameraPosition.z = -10.0f;
              
        transform.position = Vector3.Lerp (transform.position, cameraPosition, SmoothTime * Time.deltaTime);
    }
}
