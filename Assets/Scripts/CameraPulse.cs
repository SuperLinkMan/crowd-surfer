﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPulse : MonoBehaviour {

    public float pulseAmount = 0.1f;
    
    private MusicController musicController;
    private Camera thisCamera;
    
    private float initialCameraSize;
    private bool isPulsing;
    
    void Start () {
        thisCamera = GetComponent<Camera> ();
        initialCameraSize = thisCamera.orthographicSize;
        musicController = GameObject.FindGameObjectWithTag ("MusicController").GetComponent<MusicController> ();
    }
    
    void Update () {
        if (!isPulsing) {
            return;
        }
        
        float sizeDifference = musicController.timeSinceStartOfSong % (musicController.BPM/60.0f/4.0f) * pulseAmount;
        thisCamera.orthographicSize = initialCameraSize - sizeDifference;
    }
    
    public void StartPulsing () {
        isPulsing = true;
    }
    
    public void StopPulsing () {
        isPulsing = false;
        thisCamera.orthographicSize = initialCameraSize;
    }
}
