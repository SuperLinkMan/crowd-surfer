﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    public enum GameState {
        Title,
        Playing,
        GameOver
    }
    
    public GameState gameState = GameState.Title;
    public CameraPulse cameraPulse;
    
    public Canvas inGameCanvas;
    public Canvas titleCanvas;
    public Canvas gameOverCanvas;
    public Text finalScoreText;
    public Text highScoreText;
    
    public MusicController musicController;
    public CrowdAudio crowd;

	public GameObject guard;
	public Sprite[] guardTypes;
	public float securityGuardTime = 5f;
	public Transform[] guardPlacements;
    
    public Animator bandAnimator;
    
    public GameObject crowdSurfer;
    
    public Camera mainCamera;
    public CameraFollow projectorCamera;
    
    public float zoomedCameraSize = 2.4f;
    public float defaultCameraSize = 5.0f;
    public float cameraZoomTime = 3.0f;
    public float titleFadeTime = 1.5f;
    public float timeToWaitAtGameOver = 1.0f;
    
    public string[] inputs = {"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"};
    public int minimumMidiKeyValue = 48;
    public int maximumMidiKeyValue = 72;
    
    private Vector2 initialPlayerPosition;
    private float timeSinceGameOver = 0.0f;
	private List<GameObject> securityGuards = new List<GameObject>();
	private float securityGuardTimer = 0f;
    
    private GameObject[] speakers;
    
    void Start () {
        initialPlayerPosition = crowdSurfer.transform.position;
        speakers = GameObject.FindGameObjectsWithTag ("Speakers");
        StopAnimations ();
        RestartGame ();
    }
    
    void Update () {
        switch (gameState) {
            case GameState.Title:
                if (PlayerPressedButton ()) {
                    StartGame ();
                }
                break;
            case GameState.Playing:
				securityGuardTimer += Time.deltaTime;
				if (securityGuardTimer > securityGuardTime) {
					securityGuardTimer = 0f;
					// put out another guard
				GameObject securityGuard = Instantiate(guard, guardPlacements[Random.Range(0, guardPlacements.Length)].position, Quaternion.identity);
					securityGuard.GetComponent<SpriteRenderer>().sprite = guardTypes[Random.Range(0, guardTypes.Length)];
					securityGuards.Add(securityGuard);
				}
                break;
            case GameState.GameOver:
                if (PlayerPressedButton () && timeSinceGameOver > timeToWaitAtGameOver) {
                    RestartGame ();
                } else {
                    timeSinceGameOver += Time.deltaTime;
                }
                break;
            default:
                break;
        }
    }
    
    void StartGame () {
        gameState = GameState.Playing;
        StartCoroutine (FadeTitleOut (titleFadeTime));
        StartCoroutine (ZoomCameraOut (cameraZoomTime));
    }
    
    void StartGameForRealz () {
        if (cameraPulse != null) {
            cameraPulse.StartPulsing ();
        }
        titleCanvas.enabled = false;
        gameOverCanvas.enabled = false;
        inGameCanvas.enabled = true;
        
        crowdSurfer.SetActive (true);
        projectorCamera.target = crowdSurfer;
        
        musicController.StartSong ();
        StartAnimations ();
    }
    
    public void EndGame () {
        gameState = GameState.GameOver;
        if (cameraPulse != null) {
            cameraPulse.StopPulsing ();
        }
        
        titleCanvas.enabled = false;
        gameOverCanvas.enabled = true;
        inGameCanvas.enabled = false;
        StopAnimations ();
        
        musicController.StopSong ();
        crowd.PlayBoo ();
        
        int finalScore = crowdSurfer.GetComponent<SurferScore> ().GetTotalScore ();
        
        crowdSurfer.SetActive (false);
        
        int highScore = PlayerPrefs.GetInt ("highscore");
        if (highScore < finalScore) {
            PlayerPrefs.SetInt ("highscore", finalScore);
            finalScoreText.text = "High Score! " + finalScore;
            finalScoreText.color = new Color (153.0f/255.0f, 108.0f/255.0f, 244.0f/255.0f);
        } else {
            finalScoreText.text = "Final Score\n" + finalScore;
            finalScoreText.color = new Color (232.0f/255.0f, 35.0f/255.0f, 81.0f/255.0f);
        }
    }
    
    bool PlayerPressedButton () {
        foreach (string key in inputs) {
            if (Input.GetKeyDown (key)) {
                return true;
            }
        }
        
        for (int key = minimumMidiKeyValue; key <= maximumMidiKeyValue; key++) {
			try {
	            if (MidiJack.MidiMaster.GetKeyDown (key)) {
	                return true;
	            }
			} catch {} // swallow exception
        }

		// touch input
		if (Input.touchCount > 0) {
			return true;
		}
        
        return false;
    }
    
    void RestartGame () {
        gameState = GameState.Title;
        titleCanvas.enabled = true;
        gameOverCanvas.enabled = false;
        inGameCanvas.enabled = false;
        crowdSurfer.transform.position = initialPlayerPosition;
        crowdSurfer.GetComponent<SurferScore> ().ResetScore ();
        crowdSurfer.SetActive (false);
        musicController.StopSong ();
        crowd.StartCrowd ();
        mainCamera.orthographicSize = zoomedCameraSize;
		foreach (GameObject guard in securityGuards) {
			Destroy(guard);
		}
		securityGuards = new List<GameObject>();
		securityGuardTimer = 0f;
        
        int highScore = PlayerPrefs.GetInt ("highscore");
        if (highScore <= 0) {
            highScoreText.enabled = false;
        } else {
            highScoreText.enabled = true;
            highScoreText.text = "High Score: " + highScore;
        }
    }
    
    void StopAnimations () {
        foreach (GameObject speaker in speakers) {
            Animator anim = speaker.GetComponent<Animator> ();
            anim.SetBool ("IsPlaying", false);
        }
        bandAnimator.SetBool ("IsPlaying", false);
    }
    
    void StartAnimations () {
        foreach (GameObject speaker in speakers) {
            Animator anim = speaker.GetComponent<Animator> ();
            anim.SetBool ("IsPlaying", true);
        }
        bandAnimator.SetBool ("IsPlaying", true);
    }
    
    IEnumerator FadeTitleOut (float fadeTime) {
        CanvasGroup cg = titleCanvas.GetComponent<CanvasGroup> ();
        
        while (cg.alpha > 0.0f) {
            cg.alpha -= Time.deltaTime / fadeTime;
            yield return null;
        }
        
        titleCanvas.enabled = false;
        cg.alpha = 1.0f;
    }
    
    IEnumerator ZoomCameraOut (float zoomTime) {
        while (mainCamera.orthographicSize < defaultCameraSize) {
            mainCamera.orthographicSize += Time.deltaTime / zoomTime;
            yield return null;
        }
        
        mainCamera.orthographicSize = defaultCameraSize;
        StartGameForRealz ();
    }
}
