﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidiJack;

public class KeyboardController : MonoBehaviour {

    // Lownote should be F1
    public AudioClip lowNote;
    // Midnote should be F2
    public AudioClip midNote;
    // Highnote should be F3
    public AudioClip highNote;
    public AudioSource audioSource;
    
    // All are different octaves of F
    public int lowFValue = 41;
    public int middleFValue = 53;
    public int highFValue = 65;
    public int highestNoteValue = 100;
    
    private string[] inputs = {"q", "q", "w", "e", "e", "r", "r", "t", "y", "y", "u", "u", "i", "i", "o", "p", "p"};
    
    private int lowestFValue = 5;
    
    private int[] scale = {0, 0, 2, 3, 3, 5, 5, 7, 8, 8, 10, 10};
    
    private float pitchMultiplier = 1.05946f;
	
	// Update is called once per frame
	void Update () {
        bool playedNote = false;
        for (int currentNote = lowFValue; currentNote <= highestNoteValue; currentNote++) {
            if (MidiMaster.GetKeyDown (currentNote)) {
                audioSource.Stop ();
                audioSource.clip = currentNote < middleFValue ? lowNote : currentNote < highFValue ? midNote : highNote;
                audioSource.pitch = Mathf.Pow (pitchMultiplier, scale[(currentNote - lowestFValue) % 12]);
                audioSource.Play ();
                
                playedNote = true;
            }
        }
        
        for (int i = 0; i < inputs.Length; i++) {
            if (Input.GetKeyDown (inputs[i])) {
                int currentNote = i + middleFValue;
            
                audioSource.Stop ();
                audioSource.clip = currentNote < middleFValue ? lowNote : currentNote < highFValue ? midNote : highNote;
                audioSource.pitch = Mathf.Pow (pitchMultiplier, scale[(currentNote - lowestFValue) % 12]);
                audioSource.Play ();
                
                playedNote = true;
            }
        }
        
        if (playedNote) {
            CalculateRhythm();
        }
	}
    
    void CalculateRhythm () {
        
    }
}
