﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdAudio : MonoBehaviour {

    public AudioSource source;
    public AudioClip booClip;
    public AudioClip cheerClip;
	
    public void StartCrowd () {
        source.Play ();
    }
    
	public void PlayBoo () {
        source.Stop ();
        source.PlayOneShot (booClip);
    }
    
    public void PlayCheers () {
        source.PlayOneShot (cheerClip);
    }
}
