﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonProperties: MonoBehaviour {

	[HideInInspector] public float maxHeight = 0f;
	[HideInInspector] public bool animUp = true;
	[HideInInspector] public bool jumping = false;

	void Awake() {
		jumping = false;
		animUp = true;
	}

}
