﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdInput : MonoBehaviour {

	public string key;
	public float raiseHeight = 1f;
	public float raiseSpeed = 5f;

	private Rigidbody2D rb;
	private float initialHeight;

	void Awake () {
		rb = GetComponent<Rigidbody2D>();
		initialHeight = transform.position.y;
	}
	
	void FixedUpdate () {
		if (Input.GetKey(key)) {
			if (transform.position.y < initialHeight + raiseHeight) {
				rb.MovePosition(transform.position + transform.up * Time.deltaTime * raiseSpeed);
			}
		} else if (transform.position.y > initialHeight) {
			rb.MovePosition(transform.position - transform.up * Time.deltaTime * raiseSpeed);
		}
	}
}
