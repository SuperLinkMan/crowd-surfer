﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

    public int bpm = 120;
    public bool isPlaying = true;
    public float fadeOutTime = 1.0f;
    public int beatDivision = 4;
    
    public SpriteRenderer[] sideLights;
    
    private float secondsBetweenFlashes;
    private float secondsSinceLastFlash = 0.0f;
    
	void Awake () {
        secondsBetweenFlashes = bpm/60.0f/(float)beatDivision;
        secondsSinceLastFlash = secondsBetweenFlashes;
    }
    
	void Start () {
//		DisableAllLights (fadeOutTime);
	}
	
	// Update is called once per frame
	void Update () {
		if (isPlaying) {
            FlashLights();
        } else {
            
        }
	}
    
    void FlashLights () {
        secondsSinceLastFlash += Time.deltaTime;
        if (secondsSinceLastFlash > secondsBetweenFlashes) {
            secondsSinceLastFlash = 0.0f;
            foreach (SpriteRenderer sr in sideLights) {
                StartCoroutine (FadeLightOverTime (sr, false, secondsBetweenFlashes));
            }
        }
    }
    
    void DisableAllLights(float fadeTime) {
        foreach (SpriteRenderer sr in sideLights) {
            StartCoroutine (FadeLightOverTime (sr, false, fadeTime));
        }
    }
    
    IEnumerator FadeLightOverTime (SpriteRenderer sr, bool fadeIn, float fadeTime) {
        float timeRemaining = fadeIn ? 0.0f : fadeTime;
        
        while (timeRemaining >= 0.0f && timeRemaining <= fadeTime) {
            sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, timeRemaining/fadeTime);
            timeRemaining += Time.deltaTime * (fadeIn ? 1.0f : -1.0f);
            yield return null;
        }
    }
}
