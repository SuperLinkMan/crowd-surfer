﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {

    public int BPM = 120;
    
    public float timeSinceStartOfSong {
        get {
            return timeSinceStart;
        }
    }
    
    private AudioSource audioSource;
    private float timeSinceStart = 0.0f;
    
    void Awake () {
        audioSource = GetComponent<AudioSource> ();
    }
    
	// Use this for initialization
	public void StartSong () {
		audioSource.Play ();
        timeSinceStart = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if (!audioSource.isPlaying) {
            return;
        }
        
        timeSinceStart += Time.deltaTime;
	}
    
    public void StopSong () {
        audioSource.Stop ();
    }
}
