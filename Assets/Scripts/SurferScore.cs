﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SurferScore : MonoBehaviour {

    public Text scoreText;
    public Text currentMoveScoreText;
    public Text currentMoveScorePlus;
    public Text specificMoveText;
    public CrowdAudio crowdAudio;
    
    public float textFadeTime = 2.0f;
    
    public int pointsPerSecondHangTime = 50;
    public int pointsPerHighFlyer = 500;
    public int initialFlipValue = 100;
    public int pointMultiplierForSequentialFlips = 2;

    private float previousAngle = 0.0f;
    private float amountFlipped = 0.0f;
    private int numberOfFlips = 0;
    
    private bool isCurrentlyDoingTricks = false;
    
    private int currentFlipValue;
    private float currentTrickScore = 0;
    private int totalScore = 0;
    
    private CanvasGroup specificCanvasGroup;
    private CanvasGroup currentMoveCanvasGroup;
    private CanvasGroup plusCanvasGroup;

	private GameController gameController;
	
    void Awake () {
        currentFlipValue = initialFlipValue;
    }
    
    void Start () {
        specificCanvasGroup = specificMoveText.GetComponent<CanvasGroup> ();
        currentMoveCanvasGroup = currentMoveScoreText.GetComponent <CanvasGroup> ();
        plusCanvasGroup = currentMoveScorePlus.GetComponent <CanvasGroup> ();
    
        specificCanvasGroup.alpha = 0.0f;
        currentMoveCanvasGroup.alpha = 0.0f;
        plusCanvasGroup.alpha = 0.0f;

		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    
	// Update is called once per frame
	void Update () {
        float fadeDiff = Time.deltaTime / textFadeTime;
        if (isCurrentlyDoingTricks) {
            UpdateFlipStatus ();
            CalculateTimeScore ();
            UpdateCurrentScoreText ();
        } else {
            currentMoveCanvasGroup.alpha -= fadeDiff;
            plusCanvasGroup.alpha -= fadeDiff;
        }
        
        specificCanvasGroup.alpha -= fadeDiff;
	}
    
    void OnTriggerEnter2D (Collider2D other) {
        if (other.tag == "ScoreRegion") {
            currentTrickScore = 0.0f;
            isCurrentlyDoingTricks = true;
            currentFlipValue = initialFlipValue;
            previousAngle = transform.rotation.eulerAngles.z;
            amountFlipped = 0.0f;
            numberOfFlips = 0;
            EnableCurrentScoreText ();
        }
        if (other.name == "HighFlyerRegion") {
            currentTrickScore += pointsPerHighFlyer;
            DisplaySpecificMove("High Flyer! +" + pointsPerHighFlyer);
        }
		if (other.gameObject.CompareTag("Security")) {
			gameController.EndGame();
		}
    }
    
    void OnTriggerExit2D (Collider2D other) {
        if (other.tag == "ScoreRegion") {
            isCurrentlyDoingTricks = false;
            AddScoreToTotal ();
            UpdateScoreText ();
            crowdAudio.PlayCheers ();
        }
    }
    public void ResetScore () {
        totalScore = 0;
        currentTrickScore = 0.0f;
        currentFlipValue = initialFlipValue;
        previousAngle = 0.0f;
        amountFlipped = 0.0f;
        numberOfFlips = 0;
        isCurrentlyDoingTricks = false;
    }
    
    public int GetTotalScore () {
        return totalScore;
    }
    
    void CalculateTimeScore () {
        currentTrickScore += Time.deltaTime * pointsPerSecondHangTime;
    }
    
    void UpdateFlipStatus () {
        float currentAngle = transform.rotation.eulerAngles.z;
        
        float difference = (Mathf.Abs (currentAngle - previousAngle) % 360);
        if (difference >= 180.0) {
            difference -= 360.0f;
        }
        amountFlipped += difference;
        previousAngle = currentAngle;
        
        if (amountFlipped >= 180.0f) {
            amountFlipped -= 180.0f;
            numberOfFlips++;
            if (numberOfFlips > 1) {
                DisplaySpecificMove ("Flippin' Heck! +" + currentFlipValue);
            } else {
                DisplaySpecificMove ("Rad Flip! +" + currentFlipValue);
            }
            currentTrickScore += currentFlipValue;
            currentFlipValue *= pointMultiplierForSequentialFlips;
        }
    }
    
    void UpdateScoreText () {
        scoreText.text = totalScore.ToString ();
    }
    
    void EnableCurrentScoreText () {
        currentMoveCanvasGroup.alpha = 1.0f;
        plusCanvasGroup.alpha = 1.0f;
    }
    
    void UpdateCurrentScoreText () {
        currentMoveScoreText.text = ((int)currentTrickScore).ToString ();
    }
    
    void DisplaySpecificMove (string textToDisplay) {
        specificMoveText.text = textToDisplay;
        specificCanvasGroup.alpha = 1.0f;
    }
    
    void AddScoreToTotal () {
        totalScore += Mathf.RoundToInt(currentTrickScore);
    }
}
